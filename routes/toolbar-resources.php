<?php

use Illuminate\Support\Facades\Route;

Route::prefix('{resource}')
    ->group(function () {
        Route::get('', [ \Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers\ToolbarResourceController::class, 'get' ]);
        Route::post('', [ \Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers\ToolbarResourceController::class, 'post' ]);
    });

