<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools;

use Illuminate\Support\Facades\Facade;

class ToolbarTools extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return ToolbarToolsService::class; }
}
