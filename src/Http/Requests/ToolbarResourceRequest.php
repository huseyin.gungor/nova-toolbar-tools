<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Http\Requests;

use Illuminate\Support\Collection;

use Laravel\Nova\Http\Requests\NovaRequest;

use Gabelbart\Laravel\Nova\ToolbarTools\ToolbarTools;
use Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarResource;
use Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarTool;

class ToolbarResourceRequest extends NovaRequest
{
    public function getMatchingTools(): Collection
    {
        return collect(ToolbarTools::availableToolbarTools($this))
            ->filter(fn ($tool) => $this->matchesTool($tool));
    }

    public function matchesTool(ToolbarTool $tool): bool
    {
        return $tool instanceof ToolbarResource && $tool->isForModel($this->model());
    }
}
