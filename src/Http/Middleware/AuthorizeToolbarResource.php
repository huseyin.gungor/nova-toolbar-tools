<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Http\Middleware;

use Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarTool;
use Illuminate\Http\Request;

use Gabelbart\Laravel\Nova\ToolbarTools\ToolbarTools;
use Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarResource;

class AuthorizeToolbarResource
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response
     */
    public function handle(Request $request, $next)
    {
        $tool = collect(ToolbarTools::registeredToolbarTools())->first([$this, 'matchesTool']);

        return optional($tool)->authorize($request) ? $next($request) : abort(403);
    }

    /**
     * Determine whether this tool belongs to the package.
     *
     * @param \Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarTool $tool
     * @return bool
     */
    public function matchesTool(ToolbarTool $tool): bool
    {
        return $tool instanceof ToolbarResource;
    }
}
