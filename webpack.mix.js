let mix = require('laravel-mix')

mix
  .webpackConfig({
    externals: {
      "axios": "axios",
    }
  })
  .setPublicPath('dist')
  .js('resources/js/main.js', 'js')
  .sass('resources/sass/main.scss', 'css')
